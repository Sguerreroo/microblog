from flask import render_template
from microblog_app import app

@app.route('/')
@app.route('/index')
def index():
    return "Hello, World!"